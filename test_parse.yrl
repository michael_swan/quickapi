
Nonterminals
forms form
generator
predicate
boolean_operator
predicate_symbol
set_builder
generator_pattern
generators
predicate_body
model_form
model_body
relation relations
field% fields
.

Terminals
symbol
linebreak equal_indent_linebreak
'=' '<' '>' '<=' '>=' '<-'
'.' '::' '=>' 'model'
'{' '}' ':' '(' ')' ','.

Rootsymbol forms.

forms ->
    form :  ['$1'].
forms ->
    form linebreak forms :  ['$1'|'$3'].

form ->
    model_form : '$1'.
form ->
    linebreak form : '$2'.

model_form ->
    'model' symbol '=>' linebreak model_body : {model, '$2'#symbol.value, '$5'}.

%% Model Body
%% Require at least 1 field.
%% Relations are optional

%% Fields
model_body_0 ->
    field : #model{ fields = ['$1'] }.
model_body_0 ->
    field equal_indent_linebreak model_body_0 : '$3'#model{ fields =  ['$1'|'$3'#model.fields] }.
model_body_0 ->
    field equal_indent_linebreak model_body_1 : '$3'#model{ fields =  ['$1'] }.
%% Relations
model_body_1 ->
    relation : #model{ relations = ['$1'] }.
model_body_1 ->
    relation equal_indent_linebreak model_body_1 : '$3'#model{ relations = ['$1'|'$3'#model.relations] }.
model_body_1 ->
    model_body_2 : '$1'.
%% Validations
model_body_2 ->
    %% CONTINUE FROM HERE!

relation ->
    symbol '=' set_builder : {relation, '$1'#symbol.value, '$3'}.

set_builder ->
    '{' generator_pattern ':' predicate_body '}' : {set_builder, '$2', '$4'}.

field ->
    symbol '::' symbol : {type_decl, '$1'#symbol.value, '$3'#symbol.value}.

generator_pattern -> generator :  '$1'.
generator_pattern -> '(' generators ')' : '$2'.

generators -> generator_pattern :  ['$1'].
generators -> generator_pattern ',' generators :  ['$1'|'$3'].

generator ->
    symbol '<-' symbol : {generator, '$1'#symbol.value, '$3'#symbol.value}.

predicate_body ->
    predicate :  ['$1'].
predicate_body ->
    generator :  ['$1'].
predicate_body ->
    generator ',' predicate_body :  ['$1'|'$3'].
predicate_body ->
    predicate ',' predicate_body :  ['$1'|'$3'].

predicate ->
    predicate_symbol boolean_operator predicate_symbol : {predicate, element(1, '$2'), '$1', '$3'}.

boolean_operator ->
    '='  : '$1'.
boolean_operator ->
    '<'  : '$1'.
boolean_operator ->
    '>'  : '$1'.
boolean_operator ->
    '<=' : '$1'.
boolean_operator ->
    '>=' : '$1'.

predicate_symbol ->
    symbol : '$1'#symbol.value.
predicate_symbol ->
    symbol '.' symbol : {remote, '$1'#symbol.value, '$3'#symbol.value}.

Erlang code.

-include("quick_api.hrl").
