%% Error Records
-record(generic_server_error, {message}).

-record(resource_not_found, {}).
-record(model_not_found,    {model}).
-record(record_not_found,   {model, id}).

-record(invalid_content_type, {content_type}).
-record(record_exists,        {model, id, existing_record}).
