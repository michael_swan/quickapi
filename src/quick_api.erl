%% @author Michael Swan <michael@michaelswan.io>
%% @copyright 2014 Michael Swan <michael@michaelswan.io>

%% @doc quick_api.

-module(quick_api).
-include("quick_api.hrl").

-export([start/0, stop/0, pre_compile/2]).
-import(io_lib, [ format/2 ]).
-import(erl_syntax, [ attribute/2
                    , set_pos/2
		    , application/2, application/3
		    , function/2, fun_expr/1
                    , case_expr/2
                    , catch_expr/1
		    , atom/1
                    , record_access/3
		    , variable/1
		    , revert/1
		    , clause/3
		    , string/1, list/1, tuple/1, integer/1
		    , record_expr/2
		    , underscore/0, nil/0
		    , match_expr/2
		    , implicit_fun/2
		    , try_expr/2
		    , class_qualifier/2
		    , arity_qualifier/2
		    , cons/2
		    , record_expr/3, record_field/2, record_field/1
		    , if_expr/1
                    , list_comp/2, generator/2
		    , abstract/1
                    , infix_expr/3, prefix_expr/2
                    , operator/1 ]).


%% @spec start() -> ok
%% @doc Start the quick_api server.
start() ->
    {ok, _} = application:ensure_all_started(quick_api).

%% @spec stop() -> ok
%% @doc Stop the quick_api server.
stop() ->
    application:stop(quick_api).

%% @spec pre_compile(#rebar_config{}, any()) -> ok
%% @doc Stop the quick_api server.
pre_compile(RebarConf, _) ->
    rebar_base_compiler:run(RebarConf, []
                           , "src",  ".api"
                           , "ebin", ".beam"
                           , fun(SrcFile, TargetFile, _Config) ->
                                     #compiled_api{module = Mod, bytecode = Code} = quick_api_compile:file(SrcFile),
                                     Filename = atom_to_list(Mod) ++ ".beam",
                                     file:write_file(filename:append(filename:dirname(TargetFile), Filename), Code),
                                     ok
                             end
                           , [ {check_last_mod, true}
                             , {recursive,      true} ]).
