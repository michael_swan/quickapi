%% -*- erlang -*-
%%
%% %CopyrightBegin%
%% COPYRIGHT STUFF HERE!
%% %CopyrightEnd%
%%

%% Definition of the QuickAPI grammar.

Nonterminals
forms form
binding_form route_form model_form set_form
http_method  function_ref
model_header field relation
data_header
set_header set_contents
argument_list
binding_value
set_builder template qualifiers qualifier filter filter_expr filter_operator template_expr generator
fields field_head field_type tuple_type tuple_type_args
.

Terminals
'GET' 'PUT' 'POST' 'PATCH' 'DELETE'
symbol proper_symbol literal
'[' ']' ':' '=>' ',' '=' '::' ';' %'/'
'{' '}' '(' ')' '<' '>' '<=' '>=' '<-' '.' 
'model' 'set'
path.

Expect 2.

Rootsymbol forms.
%% Lint Scenarios:
%% 'GET' '[' <some path> ']' ...
%% model/set symbol begins with a capital letter and does not contain an underscore.
%% relation.model matches one of the model names.
%% 

%% TODO: Add line numbers....
forms -> form ';'       : ['$1'].
forms -> form ';' forms : ['$1'|'$3'].

form -> binding_form : '$1'.
form -> route_form   : '$1'.
form -> model_form   : '$1'.
form -> set_form     : '$1'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Binding
binding_form -> symbol '=' binding_value : #binding{ name  = '$1'#symbol.value
                                                   , line  = '$1'#symbol.line
                                                   , value = '$3' }.

binding_value -> literal           : '$1'#literal.value.
binding_value -> symbol            : '$1'#symbol.value.
binding_value -> set_contents      : '$1'.
binding_value -> symbol ':' symbol : #function_reference{ line  = '$1'#symbol.line
                                                        , value = #remote{ module   = '$1'#symbol.value
                                                                         , function = '$3'#symbol.value } }.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Route
route_form -> http_method path '=>' function_ref : #route{ method       = element(1, '$1')
                                                         , line         = line('$1')
                                                         , path         = '$2'
                                                         , function_ref = '$4' }.

http_method -> 'GET'    : '$1'.
http_method -> 'PUT'    : '$1'.
http_method -> 'POST'   : '$1'.
http_method -> 'PATCH'  : '$1'.
http_method -> 'DELETE' : '$1'.

function_ref -> symbol            : #function_reference{ line = '$1'#symbol.line
                                                       , value = '$1'#symbol.value }.
function_ref -> symbol ':' symbol : #function_reference{ line = '$1'#symbol.line
                                                       , value = #remote{ module = '$1'#symbol.value
                                                                        , function = '$3'#symbol.value }}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model
model_form -> model_header fields : '$1'#model{ fields = '$2' }.

model_header -> 'model'  data_header :  #model{ name = '$2'#proper_symbol.value
                                              , line = '$2'#proper_symbol.line }.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Fields
fields -> field            : ['$1'].
fields -> field fields : ['$1'|'$2'].

field -> field_head '=' literal : '$1'#field{ default = '$3' }.
field -> field_head             : '$1'.
field -> relation               : '$1'.

field_head -> symbol '::' field_type : #field{ name = '$1'#symbol.value
                                             , line = '$1'#symbol.line
                                             , type = '$3' }.

field_type -> proper_symbol : '$1'#proper_symbol.value.
field_type -> tuple_type : '$1'.
field_type -> '[' field_type ']' : {list, '$2'}.

tuple_type -> '(' tuple_type_args ')' : {tuple, '$2'}.

tuple_type_args -> field_head : ['$1'].
tuple_type_args -> field_head ',' tuple_type_args : ['$1'|'$3'].


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Model Relations
relation -> symbol '=' set_builder : '$3'#relation{ name = '$1'#symbol.value
                                                  , line = '$1'#symbol.line }.

set_builder -> '{' template ':' qualifiers '}' : #relation{ generators = '$2'
                                                          , predicates = '$4' }.

template -> template_expr              : '$1'.
template -> template_expr ',' template : ['$1'|'$3'].
template -> '(' template ')'           : {tuple, '$2'}.

template_expr -> generator   : '$1'.
template_expr -> filter_expr : '$1'.

qualifiers -> qualifier                : ['$1'].
qualifiers -> qualifier ',' qualifiers : ['$1'|'$3'].

qualifier -> generator : '$1'.
qualifier -> filter    : '$1'.

generator -> symbol '<-' proper_symbol : #generator{ line = '$1'#symbol.line
                                                   , variable = '$1'#symbol.value
                                                   , set = '$3'#proper_symbol.value
                                                   , type = proper }.
generator -> symbol '<-' filter_expr   : #generator{ line = '$1'#symbol.line
                                                   , variable = '$1'#symbol.value
                                                   , set = '$3'
                                                   , type = filter }.

filter -> filter_expr filter_operator filter_expr : {filter, '$1', element(1, '$2'), '$3'}.

filter_expr -> symbol            : {local, '$1'#symbol.value}.
filter_expr -> symbol '.' symbol : {remote, '$1'#symbol.value, '$3'#symbol.value}.
filter_expr -> literal           : '$1'.

filter_operator -> '='  : '$1'.
filter_operator -> '>'  : '$1'.
filter_operator -> '<'  : '$1'.
filter_operator -> '>=' : '$1'.
filter_operator -> '<=' : '$1'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set
set_form -> set_header set_contents : '$1'#set{ value = '$2' }.

set_header -> 'set' data_header : #set{ name  = '$2'#proper_symbol.value
                                      , line  = '$2'#proper_symbol.line }.

set_contents -> '[' argument_list ']' : '$2'.

argument_list -> symbol                   : ['$1'#symbol.value].
argument_list -> symbol ',' argument_list : ['$1'#symbol.value|'$3'].

data_header -> proper_symbol '=>' : '$1'.

Erlang code.

-include("quick_api.hrl").

line(Tuple) when is_tuple(Tuple) ->
    element(2, Tuple).

