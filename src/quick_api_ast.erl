-module(quick_api_ast).
-include("quick_api.hrl").
-export([translate/3, get_binding/3]).
-import(erl_syntax, [ attribute/2
                    , set_pos/2
		    , application/2, application/3
		    , function/2, fun_expr/1
                    , case_expr/2
                    , catch_expr/1
		    , atom/1
                    , record_access/3
		    , variable/1
		    , revert/1
		    , clause/3
		    , string/1, list/1, tuple/1, integer/1
		    , record_expr/2
		    , underscore/0, nil/0
		    , match_expr/2
		    , implicit_fun/2
		    , try_expr/2
		    , class_qualifier/2
		    , arity_qualifier/2
		    , cons/2
		    , record_expr/3, record_field/2, record_field/1
		    , if_expr/1
                    , list_comp/2, generator/2
		    , abstract/1
                    , infix_expr/3, prefix_expr/2
                    , operator/1 ]).

-define(api_module_exports, [ {init,0}
                            , {start,0}
                            , {start,1}
                            , {start_from_sup,1}
                            , {stop,0}
                            , {loop,1}
                            , {mjson,1}
                            , {models,0}
                            , {set_string,3}
                            , {new,1}
                            , {set,3}
                            , {update,2}
                            , {save,1}
                            , {find,2}
                            , {create,2}
                            , {field_type,2}
                            , {key_type,1}
                            ]).

-record(ast_state, {module, routes, models, sets, bindings}).

translate(Mod, File, Forms) ->
    base_ast(Mod, File) ++ complex_ast(Mod, Forms).

base_ast(Mod, File) ->
    SupAtom = atom(list_to_atom(atom_to_list(Mod) ++ "_sup")),
    StartSup = case_expr(application(atom(quick_api_sup), atom(start_link),  [list([tuple([atom(quick_api), atom(Mod), variable(port)])])]),
                                                         [clause([tuple([atom(ok), variable(pid)])],none,[ application(atom(register), [SupAtom, variable(pid)]),
                                                                                                          tuple([atom(ok), variable(pid)])]),
                                                          clause([variable(a)],none,[variable(a)])]),
    FileAttr   = attribute( atom(file), [tuple([string(File), integer(0)])]),
    ModuleAttr = attribute( atom(module), [atom(Mod)]),
    ExportAttr = attribute( atom(export), [list([ arity_qualifier(atom(A), integer(B)) || {A,B} <- ?api_module_exports])]),
    Start0Fn = function( atom(start), [ clause([], none,
                                             [ application(atom(start),  [integer(8080)])])]),
    Start1Fn = function( atom(start), [ clause([variable(port)], [[application(atom(is_integer), [variable(port)])]],
                                             [ case_expr(application(atom(whereis), [SupAtom]), [clause([atom(undefined)],none,[StartSup]),
                                                                                                 clause([variable(name)],none,
                                             [ case_expr(application(atom(is_process_alive), [variable(name)]), 
                                              [ clause([atom(false)], none, [ StartSup ]),
                                                clause([atom(true)], none, [tuple([atom(ok), variable(name)])]) 
                                             ])])])])]), 
    StartFromSupFn = function( atom(start_from_sup), [ clause([variable(options)], none,
                                             [ application(atom(mochiweb_http), atom(start),  [cons(tuple([atom(name), atom(Mod)])
                                                                                                , cons(tuple([ atom(loop)
                                                                                                             , implicit_fun(atom(loop), integer(1)) ])
                                                                                                      , variable(options)) )])])]),
    StopFn = function( atom(stop), [ clause([],none,[ case_expr(application(atom(whereis), [SupAtom]), [clause([atom(undefined)],none,[atom(not_started)]), clause([variable(pid)],none,[
                                                    application(atom(unlink), [variable(pid)]), match_expr(atom(true), application(atom(exit), [variable(pid), atom(shutdown)])), atom(ok) ])]) ])]),
    LoopFn = function( atom(loop), [ clause([ variable(request) ],none,
                                            [ try_expr( [ application( atom(route)
                                                                     , [ application( variable(request)
                                                                                    , atom(get)
                                                                                    , [atom(method)] )
                                                                       , application( atom(quick_api_controller_lib)
                                                                                    , atom(split_path)
                                                                                    , [ application( variable(request)
                                                                                                   , atom(get)
                                                                                                   , [atom(path)] ) ] )
                                                                       , variable(request) ] ) ],
                                                        [ clause([ class_qualifier( variable(type), variable(what) ) ]
                                                                , none
                                                                , [ application( atom(quick_api_controller_lib)
                                                                               , atom(route_error_response)
                                                                               , [ atom(Mod)
                                                                                 , variable(request)
                                                                                 , variable(type)
                                                                                 , variable(what) ])])])] )]),
        [ revert(Fn) || Fn <- [ FileAttr
                              , ModuleAttr
                              , ExportAttr
                              , Start0Fn
                              , Start1Fn
                              , StartFromSupFn
                              , StopFn
                              , LoopFn ] ].

complex_ast(Mod, AST) ->
    %% Separate distinct form groups
    {Routes0, Rest0} = extract_forms(route,   AST),
    {Models0, Rest1} = extract_forms(model,   Rest0),
    {Sets,   Rest2} = extract_forms(sets,    Rest1),
    {Bindings,  []} = extract_forms(binding, Rest2),
    %% Expand all routes with optional_subpaths into multiple routes with no optional_subpaths,
    %% corresponding with the possible expansions of the optional subpaths.
    %% Process routes
    Routes = [ construct_path(R) || R <- Routes0 ],
    Models = expand_field_types(Models0),
    %% TODO: Change these arguments to be encapsulated in a single record.
    S = #ast_state{ module = Mod
                  , routes = Routes
                  , models = Models
                  , sets   = Sets
                  , bindings = Bindings },
    [ revert(Fn) || Fn <- records(S) ++
                        relations(S) ++
                        [ new(S)
                        , save(S)
                        , find(S)
                        , find0(S)
                        , update(S)
                        , create(S)
                        , set(S)
                        , field_type(S)
                        , key_type(S)
                        , route(S)
                        , mjson(S)
                        , init(S)
                        , models(S)
                        , set_string(S) ]].

construct_path(R = #route{ path = #path{value = PathStr} }) ->
    R#route{path = construct_path0(PathStr)}.

construct_path0([]) -> [];
construct_path0("/:" ++ Rest0) ->
    {Var, Rest} = construct_path_variable(Rest0, []),
    [Var|construct_path0(Rest)];
construct_path0("/" ++ Rest0) ->
    {SubPath, Rest} = construct_subpath(Rest0, []),
    [SubPath|construct_path0(Rest)].

construct_path_variable([], Acc) ->
    {#variable{name =  list_to_atom(lists:reverse(Acc))}, []};
construct_path_variable([H|_] = Rest, Acc) when H == $/ ->
    {#variable{name = list_to_atom(lists:reverse(Acc))}, Rest};
construct_path_variable([H|T], Acc) ->
    construct_path_variable(T, [H|Acc]).

construct_subpath([], Acc) ->
    {lists:reverse(Acc), []};
construct_subpath([H|_] = Rest, Acc) when H == $/ ->
    {lists:reverse(Acc), Rest};
construct_subpath([H|T], Acc) ->
    construct_subpath(T, [H|Acc]).

    
expand_field_types(Models0) ->
    Names = [ {Name, Type} || #model{name = Name, fields = [#field{type = Type}|_]} <- Models0 ],
    [ M#model{fields = [ F#field{type = to_erlang_type(T, Names)} || F = #field{type = T} <- Fs]}
                || M = #model{fields = Fs} <- Models0 ].

%% IDEA: There should be an equivalent to sheriff that could be easily used with my custom macro system.
to_erlang_type(Type, ModelNames) ->
    to_erlang_type(Type, ModelNames, 100).

to_erlang_type('String', _, _) ->
    binary;
to_erlang_type('Integer', _, _) ->
    integer;
to_erlang_type('Float', _, _) ->
    float;
to_erlang_type({tuple, Ts}, ModelNames, Depth) ->
    {tuple, [{N, to_erlang_type(T, ModelNames, Depth)} || #field{name = N, type = T} <- Ts]};
to_erlang_type({list, T}, ModelNames, Depth) ->
    {list, to_erlang_type(T, ModelNames, Depth)};
to_erlang_type(_,_,0) ->
    throw(invalid_type);
to_erlang_type(Type, ModelNames, Depth) ->
    TrueType = proplists:get_value(Type, ModelNames, Type),
    case is_primitive(TrueType) of
        true ->
            TrueType;
        false ->
            to_erlang_type(TrueType, ModelNames, Depth - 1)
    end.

is_primitive(integer) -> true;
is_primitive(float) -> true;
is_primitive(binary) -> true;
is_primitive(atom) -> true;
is_primitive(_) -> false.

relations(_) ->
    []. %% TODO

records(#ast_state {models = Models}) ->
    [ model_to_record(Model) || Model <- Models ].
model_to_record(#model{ name = Name, fields = Fields }) ->
    %% TODO: Add belongs_to relation fields here as well.
    attribute( atom(record)
             , [ atom(Name), tuple([ record_field( atom(Field), abstract(Default)) || #field{ name = Field, default = Default } <- Fields ])]).

%% Function generators
new(#ast_state{models = Models}) ->
    function(atom(new), [ clause([atom(Model)],none,[record_expr(atom(Model), [])]) || #model{name = Model} <- Models ]).

save(#ast_state{models = Models}) ->
    function( atom(save)
            , [ clause( [ match_expr(variable(record), record_expr(atom(Model), [])) ]
                      , none
                      , [ default_save_body() ])
                || #model{ name = Model } <- Models ]).

default_save_body() ->
    application( atom(mnesia)
               , atom(transaction)
               , [ fun_expr([ clause([],none,[ application(atom(mnesia), atom(write), [ variable(record) ]) ]) ]) ]).

find(#ast_state{module = Mod}) ->
    function( atom(find)
            , [ clause( [variable(model), variable(id) ]
                      , none
                      , [ case_expr( catch_expr( application(atom(find0), [variable(model), variable(id)]) )
                                   , [ clause( [ tuple([ atom('EXIT')
                                                       , tuple([ atom(badarg)
                                                               , cons( tuple([ atom(erlang)
                                                                             , variable(fn)
                                                                             , underscore()
                                                                             , underscore() ])
                                                                     , cons( tuple([ atom(Mod)
                                                                                   , atom(find0)
                                                                                   , integer(2)
                                                                                   , underscore() ])
                                                                           , underscore() )) ]) ])]
                                             , [ [infix_expr(variable(fn), operator('=='), atom(A))] || A <- [ atom_to_list
                                                                                                             , integer_to_list
                                                                                                             , float_to_list
                                                                                                             , binary_to_list
                                                                                                             , atom_to_binary
                                                                                                             , integer_to_binary
                                                                                                             , float_to_binary
                                                                                                             , list_to_binary
                                                                                                             , binary_to_float
                                                                                                             , list_to_float
                                                                                                             , binary_to_integer
                                                                                                             , list_to_integer ] ]
                                             , [ list([]) ])
                                     , clause( [match_expr(variable(error), tuple([ atom('EXIT'), underscore() ]))], none, [application(atom(throw), [ variable(error) ])] )
                                     , clause( [variable('A')], none, [variable('A')]) ] ) ] ) ]).
find0(#ast_state{models = Models}) ->
    function( atom(find0)
            , lists:flatten([ find_clause(Model) || Model <- Models ])).

find_clause(#model{ name = Name, fields = [#field{ type = Type }|_] }) ->
    PossibleTypes =
        [ {list,    is_list}
        , {binary,  is_binary}
        , {integer, is_integer}
        , {atom,    is_atom} ],
    [ clause( [ atom(Name), variable(id) ]
            , application( atom(B), [variable(id)])
            , [ application(atom(quick_api_controller_lib), atom(find), [atom(Name), type_conversion_expr(variable(id), A, Type)]) ] ) || {A,B} <- proplists:delete(Type, PossibleTypes)
    ] ++ [ clause( [ atom(Name), variable(id) ]
                 , none
                 , [ application(atom(quick_api_controller_lib), atom(find), [atom(Name), variable(id)]) ]) ].

field_type(#ast_state{ models = Models }) ->
    function( atom(field_type)
            , [ clause([atom(Name), atom(Fn)], none, [abstract(Ft)]) || #model{ name = Name, fields = Fs } <- Models, #field{name = Fn, type = Ft} <- Fs ]).

key_type(#ast_state{ models = Models }) ->
    function( atom(key_type)
            , [ clause([atom(Name)], none, [abstract(Ft)]) || #model{ name = Name, fields = [#field{type = Ft}|_] } <- Models ]).

update(_) ->
    function(atom(update), [ clause([nil(), variable(record)],none,[variable(record)])
                           , clause( [ cons(tuple([variable(key), variable(value)]), variable(tail)), variable(record) ]
                                   , none
                                   , [ application( atom(update)
                                                  , [ variable(tail)
                                                    , application( atom(set)
                                                                 , [ variable(key)
                                                                   , variable(value)
                                                                   , variable(record) ])])])]).


models(#ast_state{ models = Models }) ->
    function( atom(models)
            , [ clause([], none,
                       [ list([ atom(Model) || #model{ name = Model } <- Models ])]) ] ).

create(_) ->
    function(atom(create), [ clause([ variable(model), variable(props) ], none, [
                                                                                 application( atom(save),
                                                                                              [application( atom(update)
                                                                                                          , [ variable(props), application( atom(new), [variable(model)]) ])])
                                                                                ]) ]).
%% NOTE: set and set_string assume that the value is always a list.
set(_) ->
    function( atom(set)
            , [ clause( [ variable(key), variable(value), variable(record) ]
                      , application(atom(is_atom), [variable(key)])
                      , [ application(atom(set_string), [ type_conversion_expr(variable(key), atom, list)
                                                 , variable(value)
                                                 , variable(record) ]) ] )
              , clause( [ variable(key), variable(value), variable(record) ]
                      , application(atom(is_binary), [variable(key)])
                      , [ application(atom(set_string), [ type_conversion_expr(variable(key), binary, list)
                                                 , variable(value)
                                                 , variable(record) ])])
              , clause( [ variable(key), variable(value), variable(record) ]
                      , application(atom(is_list), [variable(key)])
                      , [ if_expr([ clause( []
                                          , application(atom(is_list), [variable(value)])
                                          , [ application(atom(set_string), [variable(key), variable(value), variable(record)]) ]) ]) ]) ]).

set_string(#ast_state{ models = Models }) ->
    function( atom(set_string)
            , [ set_string_clause(Model, Field) || #model{ name = Model
                                                         , fields = Fields } <- Models,
                                                   Field <- Fields ] ).

set_string_clause(Model, #field{ name = Field, type = Type }) ->
    clause( [ string(atom_to_list(Field))
            , variable(value)
            , match_expr( variable(record)
                        , record_expr(atom(Model), []) ) ]
          , none
          , [ record_expr( variable(record), atom(Model), [ set_pos( record_field(atom(Field), type_conversion_expr( variable(value), list, Type)), 1) ])] ).

%% BEGIN ROUTE FUNCTION
route(#ast_state{module = Mod, routes = AllRoutes, models = Models, bindings = Bindings}) ->
%    AllRoutes = expand_routes(Routes),
    RouteClauses =
        [ clause( [ atom(Route#route.method)
                  , route_path_pattern(Route)
                  , variable('$Request$') ]
                , route_guard(Route, Models) %% TODO: Add user-defined guards
                , [ route_clause_body(Mod, Route) ] )
          || Route <- AllRoutes ],
    HeadClause =
        clause( [ atom('HEAD'), variable('A'), variable('B') ]
              , none
              , [ application( atom(route), [atom('GET'), variable('A'), variable('B')]) ]),
    PreFailureClause =
        clause([ atom('GET'), variable(path), variable(request) ],
               none,
               [ application( atom(quick_api_controller_lib)
                            , atom(route_static_get)
                            , [atom(Mod), variable(request), variable(path), string(binary_to_list(get_binding(Bindings, static_content, undefined)))])]),
    FailureClause =
        clause([ underscore(), underscore(), variable(request) ],
               none,
               [ application( atom(quick_api_controller_lib)
                            , atom(route_fallthrough_error_response)
                            , [atom(Mod), variable(request)])]),
    function(atom(route), RouteClauses ++ [HeadClause, PreFailureClause, FailureClause]).

expand_routes([]) -> [];
expand_routes([#route{ path = Path } = R|Rs]) ->
    [_|NewPaths] = expand_path(Path),
    [ R#route{ path = P } || P <- NewPaths ] ++ expand_routes(Rs).

%% expand_route([subpath(),...]) -> [[non_opt_subpath(),...]].
%% It takes a list of subpaths and emits a list of subpath lists.
expand_path(Path) ->
    {StaticStrs, Sets} = expand_sets(Path, [[]], []),
    %% 'StaticStrs' is a list of route sections (route section is a list of route subpaths)
    %% 'Sets' is a list where each element corresponds with an optional section. Each element is a list of paths.
    PowerSet = power_set(Sets),
    [ [] | [ lists:reverse(interleave(StaticStrs, A)) || A <- PowerSet ] ].

expand_sets([], Strs, Sets) -> {Strs, Sets};
expand_sets([{optional_subpath, _, SubPath}|T], Strs, Sets) ->
    expand_sets(T, [[]|Strs], [expand_path(SubPath)|Sets]);
expand_sets([H|T], [Hs|Strs], Sets) ->
    expand_sets(T, [[H|Hs]|Strs], Sets).

interleave([H], []) -> H;
interleave([Ha|Ta], [Hb|Tb]) ->
    Ha ++ Hb ++ interleave(Ta, Tb).

power_set([]) ->
    [[]];
power_set([H|T]) ->
    [ [A|B] || A <- H, B <- power_set(T) ].

route_guard(#route{ path = Path }, Models) ->
    [ [infix_expr( variable(VarName), operator('=='), string(underscore_proper(atom_to_list(Model))) )] || #variable{ name = VarName }  <- Path,
                                                                                        #model{ name = Model } <- Models,
                                                                                        VarName == model ].

underscore_proper(Str0) ->
    Str1 = re:replace(Str0, "([A-Z]+)([A-Z][a-z])", "\1_\2"),
    Str2 = re:replace(Str1, "([a-z\d])([A-Z])", "\1_\2"),
    string:to_lower(Str2).
    
route_path_pattern(#route{ path = Path }) ->
    list([ case SubPath of
               empty_subpath ->
                   nil();
               Str when is_list(Str) ->
                   string(Str);
               #variable{ name = VarName } ->
                   variable(VarName) %% TODO: POSSIBLE HANDLE OPTIONAL SUB-PATHS
           end || SubPath <- Path ]).
route_clause_body(APIMod, #route{ function_ref = #function_reference{ value = Fn } } = Route) when is_atom(Fn) ->
    application( atom(quick_api_default_controller)
               , atom(Fn)
               , [ atom(APIMod)
                 , variable('$Request$')
                 , route_proplist_constructor(Route) ]);
route_clause_body(APIMod, #route{ function_ref = #function_reference{ value = #remote{ module = Mod, function = Fn } } } = Route) ->
    application( atom(Mod)
               , atom(Fn)
               , [ atom(APIMod)
                 , variable('$Request$')
                 , route_proplist_constructor(Route) ]).

route_proplist_constructor(#route{ path = Path }) ->
    list([ tuple([atom(VarName), variable(VarName)])
           || #variable{ name = VarName } <- Path ]).
%% END ROUTE FUNCTION

mjson(#ast_state{models = Models}) ->
    function(atom(mjson), [ mjson_clause(Model) || Model <- Models ]).

mjson_clause(#model{ name = Name, fields = Fields0 }) ->
    Fields = [ FieldName || #field{ name = FieldName } <- Fields0 ],
    clause([ tuple([atom(Name)|
                    [ variable(Field) || Field <- Fields ]]) ],
           none,
           [ tuple([ atom(struct)
                  , list([ tuple([ atom(Field), variable(Field)]) || Field <- Fields ])])]).

init(#ast_state{models = Models}) ->
    function(atom(init), [clause([],none,[ create_table_expr(Model) || Model <- Models])]).

create_table_expr(#model{ name = Name, fields = Fields }) ->
    match_expr( atom(ok)
              , application( atom(quick_api_controller_lib)
                           , atom(ensure_table_created)
                           , [ atom(Name)
                             , list([ atom(Field)
                                      || #field{ name = Field } <- Fields ])])).


%% Expression generators
type_conversion_expr(Expr, A, A) ->
    Expr;
type_conversion_expr(Expr, _, {list, _}) ->
    Expr;
type_conversion_expr(Expr, _, {tuple, _}) ->
    Expr;
type_conversion_expr(Expr, atom, list) ->
    application(atom(atom_to_list), [Expr]);
type_conversion_expr(Expr, atom, binary) ->
    application(atom(atom_to_binary), [Expr, atom(latin1)]);
type_conversion_expr(Expr, atom, integer) ->
    application(atom(list_to_integer), [application(atom(atom_to_list), [Expr])]);
type_conversion_expr(Expr, atom, float) ->
    application(atom(list_to_float), [application(atom(atom_to_list), [Expr])]);
type_conversion_expr(Expr, binary, list) ->
    application(atom(binary_to_list), [Expr]);
type_conversion_expr(Expr, binary, integer) ->
    application(atom(binary_to_integer), [Expr]);
type_conversion_expr(Expr, binary, float) ->
    application(atom(binary_to_float), [Expr]);
type_conversion_expr(Expr, list, binary) ->
    application(atom(list_to_binary), [Expr]);
type_conversion_expr(Expr, list, integer) ->
    application(atom(list_to_integer), [Expr]);
type_conversion_expr(Expr, list, float) ->
    application(atom(list_to_float), [Expr]);
type_conversion_expr(Expr, integer, binary) ->
    application(atom(integer_to_binary), [Expr]);
type_conversion_expr(Expr, integer, list) ->
    application(atom(integer_to_list), [Expr]);
type_conversion_expr(Expr, float, binary) ->
    application(atom(float_to_binary), [Expr]);
type_conversion_expr(Expr, float, list) ->
    application(atom(float_to_list), [Expr]).


%% Helpers
get_binding([], _, Default) -> Default;
get_binding([#binding{name = VarName, value = Value}|_], VarName, _) -> Value;
get_binding([_|T], VarName, Default) -> get_binding(T, VarName, Default).

extract_forms(Rec, Forms) ->
    lists:partition(fun(A) when is_tuple(A) -> element(1, A) == Rec;
                       (_) -> false
                    end, Forms).

