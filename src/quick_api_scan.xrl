%%% File    : quick_api_scan.xrl
%%% Purpose : Token definitions for QuickAPI.

Definitions.
L       = [a-z]
U       = [A-Z]
A	= [a-zA-Z_]
NL      = [\n]
WS	= [\000-\t\v-\s]
HM      = (GET|PUT|POST|PATCH|DELETE)
SC      = ([;\.\[\]\,\|\:{}\(\)~]|=>|<-|one|many|model|set|{HM})
SP      = /\:?{A}+
MEM     = ∈

Rules.
%% Strings
"(\\\^.|\\.|[^\"])*" : Ptr = bump_pointer(TokenLen), {token, #literal{ line = TokenLine
                                                                     , first = Ptr
                                                                     , last = Ptr + TokenLen
                                                                     , value = to_string(TokenChars, TokenLen) }}.

%% Comments
--.*     : bump_pointer(TokenLen), skip_token.

%% Path segments
/[/_a-zA-Z0-9:]* : Ptr = bump_pointer(TokenLen), {token, #path{line = TokenLine, first = Ptr, last = Ptr + TokenLen, value = TokenChars}}.
%% /\:{A}+       :
%% {token, {path_variable, TokenLine, list_to_atom(TokenChars -- "/:")}}.
%% /{A}+         : {token, {path_string, TokenLine, list_to_atom(TokenChars -- "/")}}.
%% /{WS}+        : {token, {empty_subpath, TokenLine}}.

%% Integers
[0-9]+        : Ptr = bump_pointer(TokenLen), {token, #literal{ line = TokenLine
                                                              , first = Ptr
                                                              , last = Ptr + TokenLen
                                                              , value = list_to_integer(TokenChars)}}.

%% Special characters
{MEM}         : Ptr = bump_pointer(TokenLen), {token, {'<-', TokenLine, Ptr, Ptr + TokenLen}}.
::            : Ptr = bump_pointer(TokenLen), {token, {'::', TokenLine, Ptr, Ptr + TokenLen}}.
{SC}	      : Ptr = bump_pointer(TokenLen), {token, {list_to_atom(TokenChars), TokenLine, Ptr, Ptr + TokenLen}}.
=	      : Ptr = bump_pointer(TokenLen), {token, {'=',  TokenLine, Ptr, Ptr + TokenLen}}. %% '=>' must take precedence.
>	      : Ptr = bump_pointer(TokenLen), {token, {'>',  TokenLine, Ptr, Ptr + TokenLen}}.
<	      : Ptr = bump_pointer(TokenLen), {token, {'<',  TokenLine, Ptr, Ptr + TokenLen}}.
>=	      : Ptr = bump_pointer(TokenLen), {token, {'>=', TokenLine, Ptr, Ptr + TokenLen}}.
<=	      : Ptr = bump_pointer(TokenLen), {token, {'<=', TokenLine, Ptr, Ptr + TokenLen}}.

%% Symbols
{U}{A}*	      : Ptr = bump_pointer(TokenLen), {token, {proper_symbol, TokenLine, Ptr, Ptr + TokenLen, list_to_atom(TokenChars), normal}}.
{L}{A}*	      : Ptr = bump_pointer(TokenLen), {token, {symbol, TokenLine, Ptr, Ptr + TokenLen, list_to_atom(TokenChars), normal}}.

%% Linebreaks
{NL}({NL}|{WS}|--.*)* : bump_pointer(TokenLen), skip_token.

%% Whitespace
{WS}+	      : bump_pointer(TokenLen), skip_token.

Erlang code.
-include("quick_api_scan.hrl").
-export([file/1]).

file(Filename) ->
    put(scan_pointer, 0),
    {ok, Bin} = file:read_file(Filename),
    {ok, Tokens, _} = ?MODULE:string(unicode:characters_to_list(Bin)),
    Tokens.
bump_pointer(Len) ->
    put(scan_pointer, get(scan_pointer) + Len).

linebreak(Chars, Ln) ->
    T = case string:tokens(Chars, "\n") of
            [] -> [];
            Toks -> lists:last(Toks)
        end,
    Tok = case get(previous_indentation) of
              [] -> linebreak;
              T -> equal_indent_linebreak;
              _ -> linebreak
          end,
    put(previous_indentation, T),
    {token, {Tok, Ln}}.
    
to_string(TokenChars, TokenLen) ->
    list_to_binary(string_gen(lists:sublist(TokenChars, 2, TokenLen - 2))).
    
string_gen([$\\|Cs]) ->
    string_escape(Cs);
string_gen([C|Cs]) ->
    [C|string_gen(Cs)];
string_gen([]) -> [].

string_escape([O1,O2,O3|S]) when
  O1 >= $0, O1 =< $7, O2 >= $0, O2 =< $7, O3 >= $0, O3 =< $7 ->
    [(O1*8 + O2)*8 + O3 - 73*$0|string_gen(S)];
string_escape([$^,C|Cs]) ->
    [C band 31|string_gen(Cs)];
string_escape([C|Cs]) when C >= $\000, C =< $\s ->
    string_gen(Cs);
string_escape([C|Cs]) ->
    [escape_char(C)|string_gen(Cs)].

escape_char($n) -> $\n;				%\n = LF
escape_char($r) -> $\r;				%\r = CR
escape_char($t) -> $\t;				%\t = TAB
escape_char($v) -> $\v;				%\v = VT
escape_char($b) -> $\b;				%\b = BS
escape_char($f) -> $\f;				%\f = FF
escape_char($e) -> $\e;				%\e = ESC
escape_char($s) -> $\s;				%\s = SPC
escape_char($d) -> $\d;				%\d = DEL
escape_char(C) -> C.
