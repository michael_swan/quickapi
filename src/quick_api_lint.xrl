%%% File    : quick_api_scan.xrl
%%% Purpose : Token definitions for QuickAPI.

Definitions.
A	= [a-zA-Z_]
NL      = [\n]
WS	= [\000-\t\v-\s]
TWS     = [\000-\s]
HM      = (GET|PUT|POST|PATCH|DELETE)
SC      = ([\[\]\,\|\:]|=>|one|many|model|set|{HM})
SP      = /\:?{A}+

Rules.
%% Strings
"(\\\^.|\\.|[^\"])*" : skip_token.

%% Comments
--.*          : skip_token.
#.*           : skip_token.

%% Path segments
{SP}{WS}+{SP} : {token, ?invalid_path#lint_error{ line = TokenLine
                                                , length = TokenLen } }.
{HM}{WS}+[^/\000-\t\v-\s]+ : error(invalid_route, TokenChars, TokenLen, TokenLine).
//            : {token, #lint_error{ message = "Path segments may not be empty."
                                   , line = TokenLine
                                   , offset = get(char_count) 
                                   , length = TokenLen } }.
/\:{A}+       : add_char_count(TokenLen), skip_token.
/{A}+         : add_char_count(TokenLen), skip_token.
/{WS}+        : add_char_count(TokenLen), skip_token.

%% Integers
[0-9]+        : add_char_count(TokenLen), skip_token.

%% Special characters
{SC}	      : add_char_count(TokenLen), skip_token.
=	      : add_char_count(TokenLen), skip_token.

%% Symbols
{A}+	      : add_char_count(TokenLen), skip_token.

%% Linebreaks
{NL}          : put(char_count, 0), skip_token.

%% Whitespace
{WS}+	      : add_char_count(TokenLen), skip_token.

Erlang code.
-export([format_error/3, format_errors/3]).
-include("quick_api_lint.hrl").

-define(error_msg(Msg), #lint_error{ message = Msg, offset = get(char_count) }).
-define(invalid_def(Def), ?error_msg("Invalid " ++ Def ++ " definition.")).
-define(invalid_route, ?invalid_def("route")).
-define(invalid_path, ?invalid_def("path")).

add_char_count(N) ->
    Val = case get(char_count) of
              undefined ->
                  N;
              A -> A + N
          end,
    put(char_count, Val).

error(ErrName, TokenChars, TokenLen, TokenLine) ->
    Token = {token, token(ErrName, TokenChars, TokenLen, TokenLine)},
    add_char_count(TokenLen),
    Token.

token(invalid_route, Str, Len, Ln) ->
    Offset = offset_after_hm(Str),
    ?invalid_route#lint_error{ offset = get(char_count) + Offset
                    , length = Len - Offset
                    , line = Ln }.

offset_after_hm("GET" ++ Rest) ->
    offset_after_hm0(Rest, 3);
offset_after_hm("PUT" ++ Rest) ->
    offset_after_hm0(Rest, 3);
offset_after_hm("POST" ++ Rest) ->
    offset_after_hm0(Rest, 4);
offset_after_hm("PATCH" ++ Rest) ->
    offset_after_hm0(Rest, 5);
offset_after_hm("DELETE" ++ Rest) ->
    offset_after_hm0(Rest, 6).

offset_after_hm0([C|T], N) when C >= 0 andalso C =< $\s ->
    offset_after_hm0(T, N+1);
offset_after_hm0(_, N) ->
    N.

format_errors(Errors, Bin, Filename) when is_binary(Bin) ->
    Str = unicode:characters_to_list(Bin),
    format_errors(Errors, Str, Filename);
format_errors(Errors, Str, Filename) when is_list(Str) ->
    [ format_error(Err, Str, Filename) || Err <- Errors ].

format_error(#lint_error{ message = Msg
                        , line = Ln
                        , offset = Offset
                        , length = Len }, Str, Filename) ->
    io_lib:format("~s:~B:~B: ~s~n~s~n", [ Filename
                                        , Ln
                                        , Offset
                                        , Msg
                                        , text_location(Str, Ln-1, Offset, Len) ]).

text_location(Str, 0, Offset, Len) ->
    {LineText, _} = lists:splitwith(fun(C) -> C /= $\n end, Str),
    "\n\s\s\s\s" ++ LineText ++ "\n\e[1;31m" ++ lists:duplicate(Offset+4, $\s) ++ lists:duplicate(Len, $^) ++ "\e[0m";
text_location([$\n|T], Ln, Offset, Len) ->
    text_location(T, Ln-1, Offset, Len);
text_location([_|T], Ln, Offset, Len) ->
    text_location(T, Ln, Offset, Len).
