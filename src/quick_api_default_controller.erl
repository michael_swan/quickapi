-module(quick_api_default_controller).
%% Includes
-include("quick_api_errors.hrl").
%% Exports
-export([index/3, show/3, create/3, update/3, delete/3, valid_model/3, correct_model/1]).
%% Imports
-import(io_lib, [format/2]).
-import(quick_api_controller_lib, [success_response/3, error_response/3]).

correct_model([H|T]) when H >= $a, H =< $z ->
    [H + $A - $a|correct_model0(T)];
correct_model([H|T]) when H >= $A, H =< $Z ->
    [H|correct_model0(T)].

correct_model0([]) -> [];
correct_model0([A,$_,B|T]) when B >= $a, B =< $z ->
    [A, B + $A - $a|correct_model0(T)];
correct_model0([H|T]) when H >= $a, H =< $z ->
    [H|correct_model0(T)].
    
index(APIMod, Req, [{model, ModelStr}]) ->
    case valid_model(APIMod, Req, ModelStr) of
        {ok, Model} ->
            {atomic, Records} = mnesia:transaction(fun() ->
                                                           mnesia:select(Model, [{'$1',[],['$1']}])
                                                   end),
            success_response(APIMod, Req, Records);
        Error -> Error
    end.

show(APIMod, Req, [{model, ModelStr}, {id, IdStr}]) ->
    case valid_model(APIMod, Req, ModelStr) of
        {ok, Model} ->
            Records = APIMod:find(Model, IdStr),
            case {Records, mnesia:table_info(Model, type)} of
                {Lst, bag} ->
                    success_response(APIMod, Req, Lst);
                {[Rec], _} ->
                    success_response(APIMod, Req, Rec);
                {[], _} ->
                    error_response(APIMod, Req, #record_not_found{model = ModelStr, id = IdStr})
            end;
        Error -> Error
    end;
show(APIMod, Req, [{id, _} = Id, {model, _} = Model]) ->
    show(APIMod, Req, [Model, Id]).

delete(APIMod, Req, [{model, ModelStr}, {id, IdStr}]) ->
    case valid_model(APIMod, Req, ModelStr) of
        {ok, Model} ->
            KeyType = APIMod:key_type(Model),
            Id = type_cast(IdStr, KeyType),
            {atomic, Res} = mnesia:transaction(fun() ->
                                             mnesia:delete({Model, Id})
                                     end),
            case Res of
                ok ->
                    Req:ok({"application/json", mochijson2:encode({struct, [{success, true}]})});
                _ ->
                    error_response(APIMod, Req, #record_not_found{model = ModelStr, id = IdStr})
            end,
            Req:respond({200, [], mochijson2:encode({struct, [{success, true}]})});
        Error -> Error
    end;
delete(APIMod, Req, [{id, _} = Id, {model, _} = Model]) ->
    delete(APIMod, Req, [Model, Id]).

create(APIMod, Req, [{model, ModelStr}]) ->
    case valid_model(APIMod, Req, ModelStr) of
        {ok, Model} ->
            [Key|_] = mnesia:table_info(Model, attributes),
            case Req:get_header_value("content-type") of
                "application/json" ++ _ ->
                    {struct, Props} = mochijson2:decode(Req:recv_body()),
                    APIMod:find(Model, proplists:get_value(list_to_binary(atom_to_list(Key)), Props)),
                    {atomic, ok} = APIMod:create(Model, Props); %% TODO: Make this work properly.
                "application/x-www-form-urlencoded" ++ _ ->
                    Props = Req:parse_post(),
                    Id = proplists:get_value(atom_to_list(Key), Props),
                    case APIMod:find(Model, Id) of
                        [] ->
                            {atomic, ok} = APIMod:create(Model, Props),
                            Req:respond({200, [], mochijson2:encode({struct, [{success, true}]})});
                        [Rec] ->
                            error_response(APIMod, Req, #record_exists{ model = ModelStr
                                                                      , id = Id
                                                                      , existing_record = APIMod:mjson(Rec) })
                        end;
                A ->
                    error_response(APIMod, Req, #invalid_content_type{ content_type = A })
            end;
        Error -> Error
    end.

update(APIMod, Req, [{model, ModelStr}, {id, IdStr}]) ->
    case valid_model(APIMod, Req, ModelStr) of
        {ok, Model} ->
            case APIMod:find(Model, IdStr) of
                [] ->
                    error_response(APIMod, Req, #record_not_found{ model = ModelStr
                                                                 , id = IdStr });
                [Rec] ->
                    [Key|_] = mnesia:table_info(Model, attributes),
                    case Req:get_header_value("content-type") of
                        "application/json" ++ _ ->
                            {struct, Props0} = mochijson2:decode(Req:recv_body()),
                            Props = lists:keydelete(atom_to_binary(Key, latin1), 1, Props0),
                            {atomic, ok} = APIMod:save(APIMod:update(Props, Rec)),
                            Req:respond({200, [], mochijson2:encode({struct, [{success, true}]})});
                        "application/x-www-form-urlencoded" ++ _ ->
                            Props0 = Req:parse_post(),
                            Props = lists:keydelete(atom_to_list(Key), 1, Props0),
                            {atomic, ok} = APIMod:save(APIMod:update(Props, Rec)),
                            Req:respond({200, [], mochijson2:encode({struct, [{success, true}]})});
                        A ->
                            error_response(APIMod, Req, #invalid_content_type{ content_type = A })
                    end
                end;
        Error -> Error
    end;
update(APIMod, Req, [{id, _} = Id, {model, _} = Model]) ->
    update(APIMod, Req, [Model, Id]).


valid_model(APIMod, Req, ModelStr) ->
    case catch list_to_existing_atom(correct_model(ModelStr)) of
        {'EXIT',{badarg,_}} ->
            error_response(APIMod, Req, #model_not_found{model = ModelStr});
        {'EXIT',Reason} ->
            error_response(APIMod, Req, #generic_server_error{message = format("~p", [Reason])});
        Model ->
            case lists:member(Model, APIMod:models()) of
                true ->
                    {ok, Model};
                false ->
                    error_response(APIMod, Req, #model_not_found{model = ModelStr})
            end
    end.

type_cast(Str, binary) ->
    list_to_binary(Str);
type_cast(Str, integer) ->
    list_to_integer(Str);
type_cast(Str, float) ->
    list_to_float(Str);
type_cast(Str, list) ->
    Str.
