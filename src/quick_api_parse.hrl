%% Parser Records

%% Definitions
-record(route,    {line, method, path, function_ref, guard}). %% TODO: Add guards.
-record(set,      {line, name, value}).
-record(model,    {line, name, fields}).
-record(field,    {line, name, type, default}).
-record(relation, {line, name, generators, predicates}).
%% Expressions
-record(generator,{line, variable, set, type}).
-record(variable, {line, name}).
-record(binding,  {line, name, value}).
-record(function_reference, {line, value}).
-record(remote,   {line, module, function}).
-record(filter, {line, left, right, operator}).
