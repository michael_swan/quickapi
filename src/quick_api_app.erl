%% @author Michael Swan <michael@michaelswan.io>
%% @copyright quick_api Michael Swan <michael@michaelswan.io>

%% @doc Callbacks for the quick_api application.

-module(quick_api_app).
-author("Michael Swan <michael@michaelswan.io>").

-behaviour(application).
-export([start/2, stop/1]).


%% @spec start(_Type, _StartArgs) -> ServerRet
%% @doc application start callback for quick_api.
start(_Type, StartArgs) ->
    quick_api_sup:start_link(StartArgs).

%% @spec stop(_State) -> ServerRet
%% @doc application stop callback for quick_api.
stop(_State) ->
    ok.
