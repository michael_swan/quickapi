-module(quick_api_compile).
-include("quick_api.hrl").
-export([ file/1, file/2
        , forms/1, forms/2]).

file(File) ->
    file(File, [verbose, report_errors, report_warnings]).

file(File, Options) ->
    case parse_file(File, Options) of
        {ok, CustomAST} ->
            ModStr = filename:basename(filename:rootname(File, ".api")),
            Mod = list_to_atom(ModStr ++ "_api"),
            forms(CustomAST, [{module, Mod}, {file, File}|Options]);
        {error, Errors} ->
            lists:foreach(fun(Err) ->
                                  ?info("~s", [Err])
                          end, Errors)
    end.

forms(Forms) ->
    forms(Forms, [verbose, report_errors, report_warnings]).

forms(Forms, Options) ->
    case {get_value(module, Options), get_value(file, Options)} of
        {undefined, undefined} ->
            {error, "Module and file unspecified."};
        {undefined, _} ->
            {error, "Module unspecified."};
        {_, undefined} ->
            {error, "File unspecified."};
        {Mod, File} ->
            BeamForms = quick_api_ast:translate(Mod, File, Forms),
            {ok, Mod, BeamBin} = compile:forms( BeamForms
                                              , Options -- [{module, Mod}, {file, File}]),
            %% Return all data of interest.
            #compiled_api{ module   = Mod
                         , bytecode = BeamBin
                         , options  = #quick_api{ module = Mod
                                                , port = quick_api_ast:get_binding(Forms, port, 8080) } }
    end.

parse_file(File, Options) ->
    {ok, Contents} = file:read_file(File),
    parse(Contents, Options).

parse(Bin, Options) when is_binary(Bin) ->
    parse(unicode:characters_to_list(Bin), Options);
parse(Str, Options) ->
    Errors = case get_value(lint, Options) of
                 true ->
                     {ok, Errors0, _} = quick_api_lint:string(Str),
                     Errors0;
                 _ -> []
             end,
    case Errors of
        [] ->
            put(scan_pointer, 0),
            case quick_api_scan:string(Str) of
                {ok, Tokens, _} ->
                    case quick_api_parse:parse(Tokens) of
                        {ok, AST} -> {ok, AST};
                        {error, {_Ln,_,_Error}} ->
                            ok = error
                            %ErrorMsg = quick_api_parse:format_error(Error),
                            %{error, [io_lib:format("~B: ~s", [Ln, ErrorMsg])]}
                    end;
                {error, {Ln,_,Error}, _} ->
                    ErrorMsg = quick_api_scan:format_error(Error),
                    {error, [io_lib:format("~B: ~s", [Ln, ErrorMsg])]}
            end;
        _ ->
            quick_api_lint:format_errors(Errors, Str, get_value(file, Options))
    end.
