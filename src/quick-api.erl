-module('quick-api').
-export([main/1]).
-include("quick_api.hrl").

%% @spec main([string()]) -> ok
%% @doc Execute command-line commands.
main([])              -> print_usage([]);
main(["help"  |Args]) -> print_usage(Args);
main(["run"   |Args]) -> cmd(run, Args);
main(["halt"  |Args]) -> cmd(halt, Args);
main(["attach"|Args]) -> cmd(attach, Args);
main(["deploy"|Args]) -> cmd(deploy, Args);
main(["status"|Args]) -> cmd(status, Args);
main(Args) ->
    io:format("Invalid arguments: ~s~n~n", [string:join(Args, ", ")]),
    print_usage([]).

%% USAGE INFORMATION %%

%% @spec print_usage([string()]) -> ok
%% @doc Print the usage information corresponding with the given command.
print_usage([]) -> print_usage(["help"]);
print_usage([Cmd|_]) ->
    ?info("Usage: ~s", [usage(Cmd)]).

%% @spec usage(string()) -> string()
%% @doc Return the usage information corresponding with the given command.
usage("run") ->
    "quick-api run <api-name>\n"
        "Compile, load, and spawn a QuickAPI node, without detaching from the terminal.";

usage("deploy") ->
    "quick-api deploy <api-name>\n"
        "       quick-api deploy <filename>\n"
        "Compile, load, spawn, and detach a QuickAPI node.";

usage("attach") ->
    "quick-api attach [<api-name>[@<hostname>]]\n"
        "Attach a QuickAPI deployment to the current terminal with a complete Erlang shell.";

usage("halt") ->
    "quick-api halt [<api-name>[@<hostname>]]\n"
        "Safely shutdown a QuickAPI deployment.";

usage("status") ->
    "quick-api status [<api-name>[@<hostname>]]\n"
        "Display details about a running QuickAPI deployment.";
usage(_) ->
    "quick-api <command> [<argument>]\n"
        "       quick-api help <command>\n"
        "where command := { run | deploy | attach | status }\n".

%% TODO: Review and refactor the following functions...

%% COMMAND DISPATCHING %%
cmd(attach, Args) ->
    case Args of
        [Node] ->
            case re:run(Node, "([^\.@]+)(?:@(.+))?", [{capture, all_but_first, list}]) of
                {match, [API]} ->
                    attach(API, "_");
                {match, [API, Host]} ->
                    attach(API, Host);
                nomatch ->
                    io:format("Invalid argument: ~s~n~n", [Node]),
                    print_usage(["attach"])
            end;
        _ ->
            io:format("Invalid number of arguments.~n~n")
    end;
cmd(F, Args) ->
    F(Args).
    
deploy_api([Filename0]) ->
    Filename = to_api_filename(Filename0),
    ensure_file_exists(Filename),
    %% Run a separate daemon instance with the "run" sub-command.
                                                %NodeName = dot_to_dash(filename:basename(Filename)),
    NodeName = atom_to_list(filename_to_module(Filename)),
    MnesiaDir = filename:join(os:getenv("HOME"), ".quick-api"),
    os:putenv("ERL_AFLAGS", io_lib:format("-boot start_sasl -detached -heart -mnesia dir '\"~s\"'", [MnesiaDir]) ),
    Cmd = string:join([escript:script_name(), "run", Filename], " "),
    ?debug("Before os:cmd"),
    os:cmd(Cmd),
    net_kernel:start([list_to_atom(NodeName ++ "-deploy"), shortnames]),
    %% TODO: Ping the deployment and get its operational status.
    Node = list_to_atom(NodeName),%++ "@" ++ net_adm:localhost()),
    ?info("NODE: ~p", [Node]),
    lists:foreach(fun(_) ->
                          timer:sleep(500),
                          case net_adm:ping(Node) of
                              pong ->
                                  io:format("'~s' API has been deployed.~n", [Filename]), %% TODO: Replace filename with the API name.
                                  halt();
                              pang -> ok
                          end
                  end, [1,2,3,4]),
    io:format("'~s' API has failed to be deployed.~n", [Filename]),
    self() ! exit.

run_api([Filename0]) ->
    code:unstick_dir(filename:dirname(code:which(sys_core_fold))),
    code:load_file(sys_core_fold),
    Filename = to_api_filename(Filename0),
    ensure_file_exists(Filename),
    %% 1: Compile
    API = quick_api:compile_api(Filename),
    %% 2: Load
    erlang:load_module( API#compiled_api.module
                      , API#compiled_api.bytecode ),
    %% 3: Execute
    application:load({application, quick_api,
                      [{description,"quick_api"},
                       {id,[]},
                       {vsn,"1.0"},
                       {modules,[quick_api, quick_api_app, quick_api_deps,
                                 quick_api_sup, quick_api_web]},
                       {maxP,infinity},
                       {maxT,infinity},
                       {registered,[]},
                       {included_applications,[]},
                       {applications,[kernel,stdlib,crypto]},
                       {env,[{included_applications,[]}]},
                       {mod,{quick_api_app,[ API#compiled_api.options ]}},
                       {start_phases,undefined}]}),
    %% 4: Start networking
    net_kernel:start([API#compiled_api.module, shortnames]),
    %% 5: Start API server
    quick_api:start(),
    io:format("'~s' API Started.~n", [API#compiled_api.module]),
    io:format("Listening on port ~p~n", [API#compiled_api.options#quick_api.port]),
    io:format("Erlang node name: '~s'~n", [node()]),
    receive exit -> ok end.

%% 1. Start net_kernel
%% 2. Open a remote shell using RPC.
attach("_", "_") ->
    net_adm:world_list([list_to_atom(net_adm:localhost())]),
    case nodes() of
        [] ->
            io:format("The '~s' API does not appear to be running.~n", [API]); %% TODO: Change this to a named error.
        Nodes ->
            [ quick_api_manager:running_apis(Node) || Node <- Nodes ] %% running_apis should return a Ref
            
    end;
attach_api(API, Host) ->
    list_to_atom(API ++ "-api@" ++ net_adm:localhost()),
    net_kernel:start([list_to_atom(API ++ "-shell"), shortnames]),
    Res = rpc:call(, shell, server, [false, false]),
    case Res of
        {badrpc, nodedown} ->
            io:format("The '~s' API does not appear to be running.~n", [API]);
        ok ->
            ok
    end,
    self() ! exit.

halt_api([API]) ->
    API0 = list_to_atom(API ++ "-api@" ++ net_adm:localhost()),
    net_kernel:start([list_to_atom(API ++ "-shell"), shortnames]),
    case net_adm:ping(API0) of
        pang ->
            io:format("'~s' API is not running.~n", [API]),
            halt();
        pong -> ok
    end,
    rpc:call(API0, erlang, halt, []),
    lists:foreach(fun(_) ->
                          timer:sleep(500),
                          case net_adm:ping(API0) of
                              pang ->
                                  io:format("'~s' API has been halted.~n", [API]),
                                  halt();
                              pong -> ok
                          end
                  end, [1,2,3,4]),
    io:format("Halt attempt timed out.~n"),
    self() ! exit.

status_api([_API]) ->
    ?debug("Testing debug."),
    ?info("Status not implemented."),
    self() ! exit.

% Helpers
to_api_filename(API) ->
    case filename:extension(API) of
        [] ->     API ++ ".api";
        ".api" -> API
    end.


ensure_file_exists(Filename) ->
    case filelib:is_file(Filename) of
        true -> ok;
        false ->
            io:format("ERROR: ~p file not found.~n~n", [Filename]),
            {ok, Files} = file:list_dir("."),
            PossibleFiles =
                lists:filter(fun(File) ->
                                     ".api" == filename:extension(File)
                             end, Files),
            case PossibleFiles of
                [] -> ok;
                _ ->
                    io:format("Did you mean one of the following: ~s~n", [string:join(PossibleFiles, ", ")])
            end,
            halt()
    end.

filename_to_module(Filename) ->
    filename:basename(filename:rootname(Filename, ".api")).
