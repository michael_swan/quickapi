%% @author Michael Swan <michael@michaelswan.io>
%% @copyright 2014 Michael Swan <michael@michaelswan.io>

%% @doc Web server for quick_api.

-module(quick_api_web).
-author("Michael Swan <michael@michaelswan.io>").

-export([init/0, start/1, stop/0, loop/1]).

%% Error Records
-record(resource_not_found,   {}).
-record(model_not_found,      {model}).
-record(record_not_found,     {model, id}).
-record(generic_server_error, {message}).

%% External API

init() ->
    {atomic, ok} = mnesia:create_table(example, [{attributes, [id, value]}]),
    ok.

start(Options) ->
    mochiweb_http:start([{name, ?MODULE}, {loop, fun(A) -> ?MODULE:loop(A) end} | Options]).

stop() ->
    mochiweb_http:stop(?MODULE).

loop(Req) ->
    try
        dispatch(Req:get(method), split_path(Req:get(path)), Req)
    catch
        Type:What ->
            Report = ["web request failed",
                      {path, Req:get(path)},
                      {type, Type}, {what, What},
                      {trace, erlang:get_stacktrace()}],
            error_logger:error_report(Report),
            Req:respond({500, [{"Content-Type", "text/javascript"}],
                         <<"{\"error\":\"generic_server_error\"}">>})
    end.

%% GENERATED CODE
dispatch('HEAD', A, B) -> dispatch('GET', A, B);
dispatch('GET', [Model], Req) ->
    index(Req, [{model, Model}]);
dispatch('GET', [Model,Id,"test"], Req) ->
    show(Req, [{model, Model},{id, Id}]);
dispatch(_,_,Req) ->
    error_response(Req, #resource_not_found{}).

ejson(example, {_, Id, Value}) ->
    {struct, [{id, Id},{value,Value}]}.
%% END GENERATED CODE

index(Req, [{model, ModelStr}]) ->
    case catch list_to_existing_atom(ModelStr) of
        {'EXIT',{badarg,_}} ->
            error_response(Req, #model_not_found{model = ModelStr});
        {'EXIT',Reason} ->
            error_response(Req, #generic_server_error{message = io_lib:format("~p", [Reason])});
        Model ->
            Results = mnesia:transaction(fun() ->
                                                 mnesia:select(Model, [{'$1',[],['$1']}])
                                         end),
            case Results of
                {atomic, Records} ->
                    success_record_json(Req, Records);
                {aborted, {no_exists,_}} ->
                    error_response(Req, #model_not_found{model = ModelStr})
            end
    end.

show(Req, [{model, ModelStr}, {id, IdStr}]) ->
    case catch list_to_existing_atom(ModelStr) of
        {'EXIT',{badarg,_}} ->
            error_response(Req, #model_not_found{model = ModelStr});
        {'EXIT',Reason} ->
            error_response(Req, #generic_server_error{message = io_lib:format("~p", [Reason])});
        Model ->
            Id = list_to_binary(IdStr),
            Results = mnesia:transaction(fun() -> mnesia:read(Model, Id) end),
            case {Results, catch mnesia:table_info(Model, type)} of
                {{atomic, Lst}, bag} ->
                    success_record_json(Req, Lst);
                {{atomic, [Rec]}, _} ->
                    success_record_json(Req, Rec);
                {{atomic,[]}, _} ->
                    error_response(Req, #record_not_found{model = ModelStr, id = IdStr});
                {_, {aborted,{no_exists,_,_}}} ->
                    error_response(Req, #model_not_found{model = ModelStr})
            end
    end;
show(Req, [{id, _} = Id, {model, _} = Model]) ->
    show(Req, [Model,Id]).

%% Response wrappers
success_record_json(Req, Records) ->
    Req:respond({200, [{"Content-Type", "text/javascript"}], to_json(Records)}).

%% Errors
error_response(Req, Error) ->
    Status = error_status_code(Error),
    DbgMsg = case debug_message(Error) of
                 undefined -> [];
                 Message -> [{debug_message, iolist_to_binary(Message)}]
             end,
    Body = mochijson2:encode({struct, [{error, element(1, Error)} | DbgMsg]}),
    Req:respond({Status, [{"Content-Type", "text/javascript"}], Body}).

debug_message(#resource_not_found{}) ->
    undefined;
debug_message(#model_not_found{model = Model}) ->
    [<<"No model called '">>,Model,<<"' found.">>];
debug_message(#record_not_found{model = Model, id = Id}) ->
    [<<"No record in the '">>,Model,<<"' model has an ID of '">>,Id,<<"'.">>];
debug_message(#generic_server_error{message = Msg}) ->
    Msg.

error_status_code(#resource_not_found{})   -> 404;
error_status_code(#model_not_found{})      -> 404;
error_status_code(#record_not_found{})     -> 404;
error_status_code(#generic_server_error{}) -> 500.

%% Internal API

get_option(Option, Options) ->
    {proplists:get_value(Option, Options), proplists:delete(Option, Options)}.

split_path([$/]) -> [];
split_path([$/|Path]) ->
    split_path(Path, "", []).

split_path([], Buf, Acc) ->
    lists:reverse([lists:reverse(Buf)|Acc]);
split_path([$/|Rest], Buf, Acc) ->
    split_path(Rest, "", [lists:reverse(Buf)|Acc]);
split_path([C|Rest], Buf, Acc) ->
    split_path(Rest, [C|Buf], Acc).

to_json([Rec|_] = Lst) ->
    to_json(element(1,Rec), Lst);
to_json(Rec) ->
    to_json(element(1,Rec), Rec).

to_json(Model, Lst) when is_list(Lst) ->
    mochijson2:encode([ejson(Model, Rec) || Rec <- Lst]);
to_json(Model, Rec) ->
    mochijson2:encode(ejson(Model, Rec)).

