-author("Michael Swan <michael@michaelswan.io>").

-include("quick_api_parse.hrl").
-include("quick_api_scan.hrl").
-include("quick_api_lint.hrl").

-record(quick_api, {module, port}).
-record(compiled_api, {module, bytecode, options}).

-ifdef(debug_mode).
-define(debug(Fmt, Args),
	io:format("QuickAPI [DEBUG]: " Fmt "~n", Args)).
-else.
-define(debug(A, B), ok).
-endif.

-define(debug(Fmt),       ?debug(Fmt, [])).

-define(info(Fmt, Args), io:format("QuickAPI: " Fmt "~n", Args)).
-define(info(Fmt),       ?info(Fmt, [])).

-define(fatal(Fmt, Args),
               ?info(Fmt, Args),
               halt()).
-define(fatal(Fmt), ?fatal(Fmt, [])).

-import(proplists, [get_value/2, get_value/3]).
