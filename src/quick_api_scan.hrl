-record(literal, {line, first, last, type, value}).
-record(symbol, {line, first, last, value, type}).
-record(proper_symbol, {line, first, last, value, type}).
-record(path, {line, first, last, value}).
-define(token(Atom), -record(Atom, {line, first, last})).
%% ?token(':').
%% ?token('::').
%% ?token('=>').
%% %?token('set').
%% ?token('model').
%% ?token('=').
%% ?token('{').
%% ?token('}').
%% ?token('<-').
-record(range, {line, first, last, type}).
%% -record(':', {line, first, last}).
%% -record('::', {line, first, last}).
%% -record('=>', {line, first, last}).
%% -record('set', {line, first, last}).
%% -record('model', {line, first, last}).

%% Lexer Records
%-record(symbol,  {line, offset, value}).
-record(integer, {line, value}).
-record(string,  {line, value}).
%-record(literal, {line, value}).
