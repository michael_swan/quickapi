%% @author Michael Swan <michael@michaelswan.io>
%% @copyright 2014 Michael Swan <michael@michaelswan.io>

%% @doc Supervisor for the quick_api application.

-module(quick_api_sup).
-author("Michael Swan <michael@michaelswan.io>").

-behaviour(supervisor).

%% External exports
-export([start_link/1, upgrade/0]).

%% supervisor callbacks
-export([init/1]).

-include("quick_api.hrl").

%% @spec start_link() -> ServerRet
%% @doc API for starting the supervisor.
start_link(APIs) when is_list(APIs) ->
    case mnesia:create_schema([node()]) of
        ok -> ok;
        {error, {_, {already_exists,_}}} -> ok;
        {error, Error} -> erlang:error(Error)
    end,
    mnesia:start(),
    supervisor:start_link(?MODULE, APIs).

%% @spec upgrade() -> ok
%% @doc Add processes if necessary.
upgrade() ->
    {ok, {_, Specs}} = init([]),

    Old = sets:from_list(
            [Name || {Name, _, _, _} <- supervisor:which_children(?MODULE)]),
    New = sets:from_list([Name || {Name, _, _, _, _, _} <- Specs]),
    Kill = sets:subtract(Old, New),

    sets:fold(fun (Id, ok) ->
                      supervisor:terminate_child(?MODULE, Id),
                      supervisor:delete_child(?MODULE, Id),
                      ok
              end, ok, Kill),

    [supervisor:start_child(?MODULE, Spec) || Spec <- Specs],
    ok.

%% @spec init([]) -> SupervisorTree
%% @doc supervisor callback.
init(APIs) ->
    %% Call each API's init function.
    lists:foreach(fun(API) ->
                          ok = (API#quick_api.module):init()
                  end, APIs),
    %% Generate supervisor specs for each API.
    Processes = [ web_spec(API#quick_api.module, API#quick_api.port) || API <- APIs ],
    Strategy = {one_for_one, 10, 10},
    {ok,
     {Strategy, Processes}}.

web_spec(Mod, Port) ->
    WebConfig = [ {ip, {0,0,0,0}}
                , {port, Port} ],
    {Mod,
     {Mod, start_from_sup, [WebConfig]},
     permanent, 5000, worker, dynamic}.
