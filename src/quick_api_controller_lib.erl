-module(quick_api_controller_lib).
%% Includes
-include("quick_api_errors.hrl").
%% Exports
-export([ success_response/3
        , error_response/3
        , route_error_response/4
        , split_path/1
        , ensure_table_created/2
        , route_fallthrough_error_response/2
        , route_static_get/4
        , find/2 ]).

%% Success handling
success_response(APIMod, Req, Records) ->
    mochiweb_request:ok({"application/json", to_json(APIMod, Records)}, Req).

to_json(APIMod, Lst) when is_list(Lst) ->
    mochijson2:encode([ {struct, [ {A, B} || {struct, L} <- [APIMod:mjson(Rec)], {A,B} <- L, B /= undefined ]} || Rec <- Lst]);
to_json(APIMod, Rec) ->
    {struct, Props} = APIMod:mjson(Rec),
    mochijson2:encode( {struct, [{A,B}||{A,B} <- Props, B /= undefined]} ).

find(Model, Id) ->
    {atomic, Res} = mnesia:transaction(fun() ->
                                               mnesia:read(Model, Id)
                                       end),
    Res.
%% Error handling
error_response(_APIMod, Req, Error) ->
    mochiweb_request:respond({ error_status_code(Error)
                             , [{"Content-Type", "application/json"}]
                             , error_body(Error) }, Req).

error_body(Error) ->
    mochijson2:encode({struct,
                       [{error, element(1, Error)}|maybe_debug_message(Error)] ++ extra_fields(Error)}).

maybe_debug_message(Error) ->
    case debug_message(Error) of
        undefined -> [];
        Message -> [{debug_message, iolist_to_binary(Message)}]
    end.
    
debug_message(#model_not_found{model = Model}) ->
    [<<"No model called '">>,Model,<<"' found.">>];
debug_message(#record_not_found{model = Model, id = Id}) ->
    [<<"No record in the '">>,Model,<<"' model has an ID of '">>,Id,<<"'.">>];
debug_message(#generic_server_error{message = Msg}) ->
    Msg;
debug_message(#invalid_content_type{ content_type = ContentType }) ->
    [<<"Invalid 'Content-Type' of '">>, ContentType, <<"'.">>];
debug_message(#record_exists{ model = Model, id = Id }) ->
    [<<"Record of '">>, Model, <<"' model with an ID of '">>, Id, <<"' exists. This action requires that the subject record is non-existent prior to its invocation.">>];
debug_message(_) ->
    undefined.

extra_fields(#record_exists{ existing_record = Record }) ->
    [{existing_record, Record}];
extra_fields(_) ->
    [].


error_status_code(#resource_not_found{}) -> 404;
error_status_code(#model_not_found{})    -> 404;
error_status_code(#record_not_found{})   -> 404;
error_status_code(_) -> 500.

%% NOTE: All functions below this line are only intended for use with
%% generated functions and not by the users.

%% Route-level Error handing
route_error_response(APIMod, Req, Type, What) ->
            Trace = erlang:get_stacktrace(),
            Report = ["web request failed",
                      {path, Req:get(path)},
                      {type, Type}, {what, What},
                      {trace, Trace}],
            error_logger:error_report(Report),
            error_response(APIMod, Req, route_error(Type, What, Trace)).

route_error(error, undef, [{Mod,Fn,Args,_}|_]) ->
    BinMod = atom_to_binary(Mod, latin1),
    BinFn  = atom_to_binary(Fn, latin1),
    Count = list_to_binary(integer_to_list(length(Args))),
    #generic_server_error{ message = <<"Undefined function ",BinMod/binary, ":", BinFn/binary, "/", Count/binary>>};
route_error(_,_,_) -> #generic_server_error{}.

%% URL/Path splitting
split_path([$/]) -> [];
split_path([$/|Path]) ->
    split_path(Path, "", []).

split_path([], Buf, Acc) ->
    lists:reverse([lists:reverse(Buf)|Acc]);
split_path([$/|Rest], Buf, Acc) ->
    split_path(Rest, "", [lists:reverse(Buf)|Acc]);
split_path([C|Rest], Buf, Acc) ->
    split_path(Rest, [C|Buf], Acc).

%% Create persistent mnesia tables.
ensure_table_created(Name, Attributes) when is_list(Attributes) ->
    case catch mnesia:table_info(Name, attributes) of
        Attributes -> ok;
        OldAttributes when is_list(OldAttributes) ->
            create_table(Name, Attributes);
        {'EXIT', {aborted,{no_exists, _, _}}} ->
            create_table(Name, Attributes);
        {'EXIT', Reason} -> Reason
    end.

create_table(Name, Attributes) ->
            Res = mnesia:create_table(Name, [ {attributes, Attributes}
                                            , {disc_copies, [node()]} ]), %% TODO: Support multiple nodes.
            case Res of
                {atomic, ok} ->
                    ok;
                {aborted, {already_exists,_}} ->
                    ok;
                {aborted, Reason} ->
                    Reason
            end.    

route_fallthrough_error_response(Mod, Req) ->
    error_response(Mod, Req, #resource_not_found{}).

route_static_get(Mod, Req, Path0, StaticDir) ->
    Path = string:join(Path0, "/"),
    case mochiweb_util:safe_relative_path(Path) of
        undefined ->
            error_response(Mod, Req, #resource_not_found{});
        RelPath ->
            case filelib:is_file(filename:join([StaticDir, RelPath])) of
                true ->
                    mochiweb_request:serve_file(Path, StaticDir, Req);
                false ->
                    error_response(Mod, Req, #resource_not_found{})
            end
    end.
