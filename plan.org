* TODO Specify supervision tree.
* TODO Implement quick_api_manager gen_server and module.

* QuickAPI Manager
** State: List of API's on the current node (and eventually, the other nodes that either (a) serve the API or (b) co-host the mnesia database)
** Functions:
*** -record(quick_api_node, {name :: atom(), full_name :: atom(), type :: [persistent_db, memory_db, http, worker]}).
*** apis/0  -> [API, ...]
*** hosts/0 -> [{quick_api_hosts, API, [{Host, [#quick_api_node{}]}]}]
*** nodes/0 -> [{quick_api_nodes, API, [#quick_api_node{}]}]
*** ^/1     - Same as above but taken from the node corresponding with the given argument.
*** api/0   -> API
*** host/0  -> 
