PREFIX:=../
DEST:=$(PREFIX)$(PROJECT)

REBAR=rebar

.PHONY: all debug edoc test clean build_plt dialyzer app

all:
	@$(REBAR) prepare-deps
	@$(REBAR) escriptize
	@mv quick_api quick-api

install: all
	@install ./quick-api /usr/bin/quick-api
debug:
	@$(REBAR) compile -Ddebug_mode -j4
	@$(REBAR) escriptize
edoc:
	@$(REBAR) doc

test:
	@rm -rf .eunit
	@mkdir -p .eunit
	@$(REBAR) eunit

clean:
	@$(REBAR) clean
	@rm -f erl_crash.dump

app:
	@$(REBAR) -r create template=mochiwebapp dest=$(DEST) appid=$(PROJECT)
