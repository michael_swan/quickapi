QuickAPI
--------
Lorem ipsum

Goals
-----
* Single-file complete API specification.
* Single command to compile and deploy.
* Know-nothing networking and distribution.

Details
-------
If you have several nodes with a QuickAPI daemon or Erlang instance is running, just provide an IP or domain name and all contacted nodes share HTTP load.

Example Syntax
-------------
```
GET   /:model     => index
GET   /:model/:id => show
PUT   /:model/:id => update
PATCH /:model/:id => update
POST  /:model/:id => create

GET / => ecc_route:schema

GET /:service_name => ecc_route:service_links

----------- Models -----------

model User =>
    username | string
    name     | string
    -----------------------
    one  | manager | User
    many | tenants | Tenant

model Tenant =>
    name | string
    -------------------------
    one  | owner    | Company
    many | services | Service

set ServiceName => [ chat, email ]

----------- Authorization -----------

-- TODO!
```
