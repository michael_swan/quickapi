-module(test2_parse).
-export([parse/1]).
-include("quick_api.hrl").

%% Output
% Forms
%-record(route, {index, run_length, checklist, path, body}).
%-record(binding, {index, run_length, checklist, variable, value}).

% Sub-Forms
-record(route_body, {run_length, checklist, line, reference}).

% Error
-record(error, {text, first, last}).

parse(Tokens) -> start(Tokens).

start(Tokens) ->
    %% Sort errors by {Err#error.line, Err#error.offset} for all errors, Err.
    Bindings = bindings(Tokens, 0, 0),
    Routes   = routes(Tokens, 0, 0),
    %% ...
    Forms = Bindings ++ Routes,
    {extract_errors(Forms), unaccepted_tokens(Tokens, Forms), overlapping_forms(Forms)}. %% Overlapping forms suggests an error in one or more form extractors.

unaccepted_tokens(Tokens, Forms) ->
    Fs = lists:sort([{element(2, Form), element(2, Form) + element(3, Form)} || Form <- Forms]),
    Is = unaccepted_tokens0(0, Fs),
    %% CONTINUE FROM HERE.

unaccepted_tokens0(_, []) -> [];
unaccepted_tokens0(_, []) -> [];
unaccepted_tokens0(A, [{A, NextA}|Fs]) ->
    unaccepted_tokens0(NextA, Fs);
unaccepted_tokens0(A, [{B, NextA}|Fs]) ->
    [{A,B}|unaccepted_tokens0(NextA, Fs)].

    
    
%% Check that no two forms share the same line

extract_errors([]) -> [];
extract_errors([#route{ checklist = CL, body = #route_body{ checklist = BodyCL} }|T]) ->
    checklist_to_errors(CL) ++ checklist_to_errors(BodyCL) ++ extract_errors(T);
extract_errors([#binding{ checklist = CL }|T]) ->
    checklist_to_errors(CL) ++ extract_errors(T).

checklist_to_errors(CL) ->
    [ Err || {false, Err = #error{}} <- CL ] ++ [ Err || Err = #error{} <- CL ].
bindings([], _, _) -> [];
bindings([ #symbol{line = VarLine, first = VarBegin,  value = Var}
         , {'=',_,_,_}
         , #literal{line = ValueLine, last = ValueEnd, value = Value} | T ], I, PrevLine) when VarLine /= PrevLine ->
    CL = [{VarLine == ValueLine,
           #error{ text = "Unexpected linebreak in binding."
                 , first = VarBegin
                 , last = ValueEnd }}],
    [ #binding{ index = I
              , run_length = 3
              , variable = Var
              , value = Value
              , checklist = CL } | bindings(T, I + 3, ValueLine) ];
bindings([H|T], I, _) ->
    bindings(T, I+1, line(H)).

routes([], _, _) ->
    [];
routes([ #proper_symbol{line = MethodLine, first = MethodBegin, value = Method}
       , #path{value = PathStr}
       , #'=>'{line = ArrowLine, last = ArrowEnd} | T], I, PrevLine)
  when PrevLine /= MethodLine, Method == 'GET' orelse Method == 'PUT' orelse Method == 'POST' orelse Method == 'PATCH' orelse Method == 'DELETE' ->
    Err = case T of
              [#symbol{line = BodyLine, first = BodyBegin}|_] ->
                  {ArrowLine == BodyLine,
                   #error{ text = "Unexpected linebreak between a route's head and body."
                         , first = ArrowEnd
                         , last  = BodyBegin }};
              _ ->
                   #error { text = "Malformed route body."
                          , first = ArrowEnd
                          , last = eol }
          end,
    {Body, Rest, NewPrevLine} = route_body(T),
    Len = 3 + Body#route_body.run_length,
    [ #route{ index = I, run_length = Len, path = route_path(PathStr), body = Body 
            , checklist = [ {MethodLine == ArrowLine,
                             #error{ text = "Unexpected linebreak within a route's head."
                                   , first = MethodBegin
                                   , last = ArrowEnd }}
                          , Err ] } | routes(Rest, I + Len, NewPrevLine) ];
routes([H|T], I, _PrevLine) ->
    routes(T, I + 1, line(H)).

route_body([ #symbol{line = ModLine, first = ModBegin, last = ModEnd, value = Mod}
           , #':' {first = ColonBegin, last = ColonEnd}
           , #symbol{line = FunLine, first = FunBegin, last = FunEnd, value = Fn}|T]) ->

    CL = [ {ModLine == FunLine,
            #error{ text = "Unexpected linebreak in a callback reference."
                  , first = ModBegin
                  , last  = FunEnd }}
         , {ModEnd == ColonBegin,
            #error{ text = "Unexpected whitespace between a callback reference's module name and it's ':' token."
                  , first = ModEnd
                  , last = ColonBegin }}
         , {ColonEnd == FunBegin,
            #error{ text = "Unexpected whitespace between a callback reference's ':' token and it's function name."
                  , first = ColonEnd
                  , last = FunBegin }}],
    {#route_body{ run_length = 3
                , checklist = CL
                , line = ModLine
                , reference = {remote, Mod, Fn} }, T, FunLine};
route_body([#symbol{line = ActionLine, value = LocalFn}|T]) ->
    {#route_body{ run_length = 1
                , checklist = []
                , line = ActionLine
                , reference = {local, LocalFn} }, T, ActionLine};
route_body([H|_] = T) ->
    {#route_body{ run_length = 0
                , checklist = []}, T, line(H) - 1}.

route_path(PathStr) ->
    PathStr.

line(T) when is_tuple(T) ->
    element(2, T).

%%% New Strategy
forms(L) ->
    {Form, T} = form(L),
    [Form|forms(T)].

form([#'model'{}|T]) ->
    %% Model
    model(T);
form([#'set'{}|T]) ->
    %% Set
    set;
form({Method,_,_,_})
  when Method == 'GET'; Method == 'PUT';
       Method == 'POST'; Method == 'PATCH';
       Method == 'DELETE' ->
    route;
form([#symbol{}|T]) ->
    binding.


model([#symbol{value = Name}, #'=>'{}|T]) ->
    model0(Toks).

form([model|T], [_|Toks]) ->
    model(Toks);
form([set|T], [_|Toks]) ->
    set(Toks);
form([Method, proper_symbol, '=>'|T], [_,_,_|Toks])
    when Method == 'GET'; Method == 'PUT';
         Method == 'POST'; Method == 'PATCH';
         Method == 'DELETE' ->
    Name = Sym#proper_symbol.value,
    route(Toks);
form([symbol, '=', literal|T], Toks) ->
    {binding, binding(Toks)}.

binding([#symbol{value = Name}, #'='{}, Literal|T]) ->
    %% BINDING
    #binding{name = Name, value = Literal#literal.value}.

%% Field
model0([#symbol{value = Name, line = L}, #'::'{}, #proper_symbol{value = Type, line = L}, #'='{}, #literal{type = Type}|T]) ->
    [#field{name = Name, type = Type}|model0(T)];
model0([#symbol{value = Name, line = L}, #'::'{}, #range{type = Type, line = L}, #'='{}, #literal{type = Type}|T]) ->
    [#field{name = Name, type = Type}|model0(T)];
model0([#symbol{value = Name, line = L}, #'::'{}, #proper_symbol{value = Type, line = L}|T]) ->
    [#field{name = Name, type = Type}|model0(T)];
model0([#symbol{value = Name}, #'='{}, #'{'{}|_] = L) ->
    model1(L);
model0(_) -> [].

%% Relation
model1(L) ->
    {OutputToks, Rest0} = lists:splitwith(fun(#':'{}) -> false;
                                            (_) -> true
                                         end, L),
    {FilterToks, Rest} = lists:splitwith(fun(#'}'{}) -> false;
                                            (_) -> true
                                         end, L),
    RelOut = relation_output(OutputToks), 
    RelFilter = relation_filter(FilterToks).

relation_output([#symbol{value = Var}, #'<-'{}, #proper_symbol{value = Set}]) ->
    [ A | A <- generator(T) ].


%    #generator{variable = Var, set = Set}.


