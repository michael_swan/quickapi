%% -*- erlang -*-
%%
%% %CopyrightBegin%
%% COPYRIGHT STUFF HERE!
%% %CopyrightEnd%
%%

%% Definition of the QuickAPI grammar.

Nonterminals
forms form
binding_form route_form model_form set_form
http_method route_path function_ref
route_subpath
model_header field_list field relation_list relation
data_header
set_header set_contents
argument_list
%relation_quantity
any_linebreak level_linebreak
special_symbol valid_atom
binding_value literal
any_linebreaks
set_builder set_builder_generators set_builder_generator set_builder_predicates set_builder_predicate set_builder_predicate_operator set_builder_predicate_expr
.

Terminals
linebreak equal_indent_linebreak
'GET' 'PUT' 'POST' 'PATCH' 'DELETE'
symbol integer string
'[' ']' ':' '=>' ',' '|' '=' '::' %'/'
'{' '}' '(' ')' '<' '>' '<=' '>=' '<-' '.' 
'model' 'set'
'one' 'many'
empty_subpath
path_string path_variable.

Expect 2.

Rootsymbol forms.
%% Lint Scenarios:
%% 'GET' '[' <some path> ']' ...
%% model/set symbol begins with a capital letter and does not contain an underscore.
%% relation.model matches one of the model names.
%% 

%% TODO: Add line numbers....
forms -> form : ['$1'].
forms -> form forms : ['$1'|'$2'].

form -> binding_form   : '$1'.
form -> route_form     : '$1'.
form -> model_form     : '$1'.
form -> set_form       : '$1'.
form -> form any_linebreaks : '$1'.

any_linebreak -> linebreak : '$1'.
any_linebreak -> equal_indent_linebreak : '$1'.

any_linebreaks -> any_linebreak : '$1'.
any_linebreaks -> any_linebreaks any_linebreak : '$2'.

level_linebreak -> equal_indent_linebreak : '$1'.
level_linebreak -> any_linebreaks equal_indent_linebreak : '$2'.

binding_form -> symbol '=' binding_value : #binding{ name = '$1'#symbol.value
                                                   , line = '$1'#symbol.line
                                                   , value = '$3' }.
literal -> valid_atom : '$1'#symbol.value.
literal -> integer : '$1'#integer.value.
literal -> string : '$1'#string.value.

binding_value -> literal : '$1'.
binding_value -> set_contents : '$1'.
binding_value -> symbol ':' symbol : #function_reference{ line = '$1'#symbol.line
                                                        , value = #remote{ module = '$1'#symbol.value
                                                                         , function = '$3'#symbol.value } }.

route_form -> http_method route_path '=>' function_ref : #route{ http_method  = element(1, '$1')
                                                               , line         = line('$1')
                                                               , path         = '$2'
                                                               , function_ref = '$4' }.

http_method -> 'GET'    : '$1'.
http_method -> 'PUT'    : '$1'.
http_method -> 'POST'   : '$1'.
http_method -> 'PATCH'  : '$1'.
http_method -> 'DELETE' : '$1'.

route_path -> route_subpath : ['$1'].
route_path -> '[' route_path ']' : [{optional_subpath, line('$1'), '$2'}].
route_path -> route_path route_path : '$1' ++ '$2'.


% url_string = contiguous string of non-whitespace alphanumeric(also any of "-_.,{}[]"... really any that are accepted in URL's except "%/:") characters.
% url_atom = same as url_string, but must begin with a letter.
route_subpath -> empty_subpath : empty_subpath.
route_subpath -> path_string   : {_,Ln,Str} = '$1',
                 #symbol{ line = Ln
                        , value = Str }.
route_subpath -> path_variable : {_,Ln,Var} = '$1',
                 #variable{ line = Ln
                          , name = Var }.
                                                             


valid_atom -> symbol         : '$1'.
valid_atom -> http_method    : to_symbol('$1').
valid_atom -> special_symbol : to_symbol('$1'). 

special_symbol -> 'model' : '$1'.
special_symbol -> 'set'   : '$1'.
special_symbol -> 'one'   : '$1'.
special_symbol -> 'many'  : '$1'.

function_ref -> symbol            : #function_reference{ line = '$1'#symbol.line
                                                       , value = '$1'#symbol.value }.
function_ref -> symbol ':' symbol : #function_reference{ line = '$1'#symbol.line
                                                       , value = #remote{ module = '$1'#symbol.value
                                                                        , function = '$3'#symbol.value }}.

model_form -> model_header field_list level_linebreak relation_list :
                  io:format("Model FL + RL~n"),
                  #model{ name      = '$1'#symbol.value
                        , line      = '$1'#symbol.line
                        , fields    = lists:reverse('$2')
                        , relations = lists:reverse('$4') }.
model_form -> model_header field_list :
              io:format("Model FL~n"),
              #model{ name   = '$1'#symbol.value
                    , line   = '$1'#symbol.line
                    , fields = lists:reverse('$2') }.
model_form -> model_header relation_list :
                                io:format("Model RL~n"),
                  #model{ name      = '$1'#symbol.value
                                      , line      = '$1'#symbol.line
                                      , relations = lists:reverse('$2') }.

%% 'model, 'User, '=>, linebreak
model_header -> 'model'  data_header linebreak : '$2'.

field_list -> field :  ['$1'].
field_list -> field_list level_linebreak field : io:format("Here~n"), S = ['$3'|'$1'], io:format("State: ~p~n",  [S] ), S.

field -> symbol '::' symbol '=' literal : io:format("Single field evaled with default~n"), #field{ name = '$1'#symbol.value
                                             , line = '$1'#symbol.line
                                             , type = '$3'#symbol.value
                                             , default = '$5' }.
field -> symbol '::' symbol : io:format("Single field evaled~n"), #field{ name = '$1'#symbol.value
                                             , line = '$1'#symbol.line
                                             , type = '$3'#symbol.value }.

relation_list -> relation : io:format("In Relation List: ~p~n",  ['$1'] ), ['$1'].
relation_list -> relation_list level_linebreak relation : ['$3'|'$1'].

relation -> symbol '=' set_builder : '$3'#relation{ name = '$1'#symbol.value
                                                  , line = '$1'#symbol.line }.

set_builder -> '{' set_builder_generators ':' set_builder_predicates '}' : #relation{ generators = lists:reverse('$2')
                                                                                    , predicates = lists:reverse('$4') }.
set_builder_generators -> set_builder_generator : ['$1'].
set_builder_generators -> set_builder_generators ',' set_builder_generator :  ['$3'|'$1'].
set_builder_generators -> '(' set_builder_generators ')' : {tuple, '$2'}.

set_builder_generator -> symbol '<-' symbol : #generator{ line = '$1'#symbol.line
                                                                       , variable = '$1'#symbol.value
                                                                       , model = '$3'#symbol.value }.

set_builder_predicates -> set_builder_predicate :  ['$1'].
set_builder_predicates -> set_builder_predicates ',' set_builder_predicate :  ['$3'|'$1'].

set_builder_predicate -> set_builder_predicate_expr set_builder_predicate_operator set_builder_predicate_expr :
                             {'$1', element(1, '$2'), '$3'}.

set_builder_predicate_operator -> '=' : '$1'.
set_builder_predicate_operator -> '>' : '$1'.
set_builder_predicate_operator -> '<' : '$1'.
set_builder_predicate_operator -> '>=' : '$1'.
set_builder_predicate_operator -> '<=' : '$1'.

set_builder_predicate_expr -> symbol : {local, '$1'#symbol.value}.
set_builder_predicate_expr -> symbol '.' symbol : {remote, '$1'#symbol.value, '$3'#symbol.value}.

set_form -> set_header set_contents : #set{ name  = '$1'#symbol.value
                                          , line  = '$1'#symbol.line
                                          , value = '$2' }.
set_header -> 'set' data_header : '$2'.

set_contents -> '[' argument_list ']' : '$2'.

argument_list -> symbol : ['$1'#symbol.value].
argument_list -> symbol ',' argument_list : ['$1'#symbol.value|'$3'].

data_header -> symbol '=>' : '$1'.

Erlang code.

-include("quick_api.hrl").

line(Tuple) when is_tuple(Tuple) ->
    element(2, Tuple).

to_symbol({Value, Ln}) ->
    #symbol{ value = Value, line = Ln }.
to_variable(#symbol{ value = Value, line = Ln }) ->
    #variable{ name = Value, line = Ln }.
