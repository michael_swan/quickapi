-module(`Mod).
-export([ init/0, start/1, stop/0, loop/1, new/1, mjson/1
        , models/0, set_string/3, set/3, update/2, save/1
        , find/2, create/2 ]).

-record(`Mod, {model}).
-record(`Model, { Field || #field{ name = Field } <- Fields })
|| #model{ name = Model, fields = Fields } <- Models.

start(Opts) ->
    mochiweb_http:start([{name, `Mod}, {loop, fun loop/1} | Opts]).

stop() ->
    mochiweb_http:stop(`Mod).

loop(Req) ->
    try
        route( Req:get(method), quick_api_controller_lib:split_path(Req:get(path)), Req )
    catch
        Type:What ->
            quick_api_controller_lib:route_error_response(`Mod, Req, Type, What)
    end.

new(`Model) -> {`Mod, #`Model{}} || #model{ name = Model } <- Models.

save({`Mod, Record = #`Model{}}) ->
    mnesia:transaction(fun() ->
                               mnesia:write(Record)
                       end)
    || #model{ name = Model } <- Models.

%% TODO: Investigate how #model.type is determined. It should be given an atom which represents the type of the primary key.
find(Id, {`Mod, Record}) when is_tuple(Record) ->
    find(element(1, Record), Id);
find(`Model, Id) when 'is_' <> `InputType(Id) ->
    quick_api_controller_lib:find( `Model
                                 , `type_cast(Id, `InputType, `Type) )
    || InputType <- proplists:delete(Type, [list, binary, float, integer])
    || #model{ name = Model, type = Type } <- Models.

update([], Record) -> Record;
update([{Key, Value}|T], Record) ->
    update(T, set(Key, Value, Record)).

set(Key, Value, Record) when is_atom(Key) ->
    set0(atom_to_list(Key), Value, Record);
set(Key, Value, Record) when is_binary(Key) ->
    set0(binary_to_list(Key), Value, Record);
set(Key, Value, Record) when is_list(Key) ->
    set0(Key, Value, Record).

%% NOTE: There should be a type_cast clause like so: type_cast(Value, A, A) -> Value.
set0(@to_list(`Key), Value, Record = #`Model{}) when 'is_' <> `ValueType(Value) ->
    {`Mod, #`Model{ `Key = @type_cast(Value, `ValueType, `Type) }}
    || #field{ name = Key, type = Type } <- Fields
    || #model{ name = Model, fields = Fields } <- Models
    || ValueType <- [ list, float, binary, integer ].

%% Accessors %%
`Field({`Mod, #`Model{ `Field = Value }}) ->
    Value
    || #field{ name = Field } <- Fields
    || #model{ name = Model, fields = Fields } <- Models.

%% belongs_to relations
`Relation({`Mod, #`Model{ `Key = Id }}) ->
    Query = qlc:q([Record || Record <- mnesia:table(`Model <> '_' <> `Relation)]),
    mnesia:transaction(fun() -> qlc:e(Query) end)
    || #relation{ name = Relation, quantity = one, model = ForeignModel } <- Relations
    || #model{ name = Model, relations = Relations } <- Models.

mjson(#`Model{ `Field = ~Field || #field{ name = Field } <- Fields }) ->
    {struct, `[ {`Field, ~Field} || #field{ name = Field } <- Fields ]}
    || #model{ fields = Fields } <- Models.


